FROM node:18-alpine

WORKDIR /opt/app

ENV PORT=1880

COPY package*.json ./
COPY public public
COPY nodered nodered
COPY index.js .

RUN npm install --omit=dev

CMD npm start
