const http = require('http');
const express = require("express");
const RED = require("node-red");

const app = express();
const server = http.createServer(app);

app.use("/", express.static("public"));

const noAdmin = process.env.NO_ADMIN || false;
const password = process.env.PASSWORD || false;
const env = process.env.ENV || 'prod';

const level = {
    20: 'error',
    30: 'warn',
    40: 'info'
};

const settings = {
    httpAdminRoot: "/nodered",
    httpNodeRoot: "/",
    userDir: "./nodered/",
    flowFile: "flows.json",
    functionGlobalContext: {},
    httpRequestTimeout: 1 * 60 * 60 * 1000, // 1 hour (in ms)
    tours: false
};


if (password) {
    settings.adminAuth = {
        type: "credentials",
        users: [
            {
                username: "admin",
                password: password,
                permissions: "*"
            }
        ]
    }
};

RED.init(server, settings);

if (!noAdmin) {
    app.use(settings.httpAdminRoot, RED.httpAdmin);
}

app.use(settings.httpNodeRoot, RED.httpNode);

server.listen(1880);

RED.start();
