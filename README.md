# c4hou



## Getting started

This project is a simple web application to validate nginx saml solution.

It is also use to validate feature in the Reverse Proxy Service.

## How to play

### Code install

```
git clone https://code.europa.eu/digit-c4/c4hou.git
npm install
npm start
```

### Docker install

```
docker run -p 1880:1880 code.europa.eu:4567/digit-c4/c4hou:latest
```

### Demo

Just want to play : [play here](https://c4hou.ntx.lu/)

## Contribute

if you find a bug or wants a feature pls refer to our [contributing page](https://code.europa.eu/digit-c4/c4hou/-/blob/main/CONTRIBUTING.md)

## What next

Not much to do except playing.
Open up your browser http://localhost:1880

"Not really used as is" unless to show nginx saml usage and punch some colleagues :)
